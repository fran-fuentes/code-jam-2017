-- Baa-ram-ewe, Baa-ram-ewe. To your breed, your fleece, your clan be True
-- Baa-ram-ewe
---     Babe
import Data.Char
import Data.List

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> String -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ res

solveAll :: [String] -> [String]
solveAll = map (solveOne . read)

-- solveOne: resolve only one number and returns the string
solveOne :: Int -> String
solveOne 0 = "INSOMNIA"
solveOne x = show $ snd . head $ dropWhile (not . and . fst) (iterate (ajust x) (replicate 10 False, 0))

ajust :: Int -> ([Bool], Int) -> ([Bool], Int)
ajust x (a, b) = (zipWith (||) a (splitBool $ show $ b + x), b + x)

splitBool :: String -> [Bool]
splitBool = foldl' t (replicate 10 False)

t :: [Bool] -> Char -> [Bool]
t b a = zipWith (||) m b
  where m = replicate (digitToInt a) False ++ [True] ++ replicate(9 - digitToInt a) False
