-- Baa-ram-ewe, Baa-ram-ewe. To your breed, your fleece, your clan be True
-- Baa-ram-ewe
---     Babe
import Data.Char
import Numeric

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> [String] -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ unlines res

solveAll :: [String] -> [[String]]
solveAll [] = []
solveAll (a:resto) = solveOne (map read (words a)) : solveAll resto

-- solveOne: resolve only one number and returns the string
solveOne :: [Int] -> [String]
solveOne [] = [""]
solveOne [_] = [""]
solveOne (n:j:_) = two
  where one     = repeat '1' : reverse (take (n-2) $ map (\x -> cycle $ replicate (2^x) '0' ++  replicate (2^x) '1') [0..])
        two     = one ++ [repeat '1']
        lnum    = map (\x -> map (head . drop x) two) [0..]
        mapmap = zipWith (\a b -> a : map (`fromBase` a) b) (repeat [2..10]) lnum


mapel :: [Int] -> String -> [String]
mapel [] cad = []
mapel (f:rest) cad = "as" : mapel rest cad
  where a = fromBase f cad
        --b = fst . head $ dropWhile (\(_, b) -> b /= 0)  (map (\x->(\x t -> (x, t mod x)) x a) listPrimes )

a :: Int
a = replicateM

fromBase :: Int -> String -> Int
fromBase base = fst . head . readInt base ((<base).digitToInt) digitToInt

listPrimes :: [Int]
listPrimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229]
